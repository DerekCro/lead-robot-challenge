#!/usr/bin/python3
import time  # Just using this to calculate execution time


def create_word_dict():
    # Create a dictionary grouping words by length
    words = [x for x in open('enable1.txt').read().split('\n')
             if len(x) > 0]
    max_word_len = max(len(x) for x in words)
    word_dict = {x + 1: set() for x in range(max_word_len)}
    for word in words:
        word_dict[len(word)].add(word)
    return words, word_dict


def variations(word):
    # Get all words in the series
    return {word[:x] + word[x+1:] for x in range(len(word))}


def funnel(word_dict, word, depth=1):
    # Calculate the funnel length
    word_variations = variations(word)
    possible_words = word_dict[len(word) - 1]
    funneled_words = possible_words & word_variations
    if len(funneled_words) == 0:
        return depth
    # Return maximum series length
    return max(funnel(word_dict, x, depth + 1) for x in funneled_words)


def bonus(word_dict):
    # Sort dictionary in reverse order to increase performance
    for word_len in reversed(sorted(word_dict.keys())):
        for word in word_dict[word_len]:
            # Run function to get maximum series length of all words
            # Since the dictionary is sorted in reverse order and there
            # is only one word that starts a funnel of 10, this word's
            # length will be returned first.
            # Check if the returned length matches the expected result and
            # return the word if true
            if funnel(word_dict, word) == 10:
                return word  # Return the word that starts funnel length of 10


def bonustwo(word_dict):
    word_count = {}
    for word_len in reversed(sorted(word_dict.keys())):
        for word in word_dict[word_len]:
            if funnel(word_dict, word) == 9:
                # Count number of words which start a funnel length of 9
                word_count[word] = 1
    return len(word_count)  # Return the correct answer


def main():
    w, wd = create_word_dict()
    # Run tests
    print(funnel(wd, "gnash"))
    print(funnel(wd, "princesses"))
    print(funnel(wd, "turntables"))
    print(funnel(wd, "implosive"))
    print(funnel(wd, "programmer"))
    print("The word that starts a funnel of length 10 is " + bonus(wd))
    print(str(bonustwo(wd)) + " words have a funnel length of 9")


start_time = time.time()  # Set current time as start time in variable
main()  # Run main function
# Subtract current time from start time and output difference
print("---\n\n Function took %s seconds to complete \n\n---"
      % (time.time() - start_time))
